class WC:
    def __init__(self, namafile):
        self.txt = namafile

    def word_count(self):
        self.f = open(self.txt,'rt')
        num_words = 0

        for line in self.f:
            num_words += len(line.split(' '))

        self.f.close()
        return num_words

    def line_count(self):
        self.f = open(self.txt,'rt')
        num_lines = 0

        for line in self.f:
            num_lines += 1
        
        self.f.close()
        return num_lines

    def char_count(self):
        self.f = open(self.txt,'rt')
        num_char = 0

        for line in self.f:
            num_char += len(line)

        self.f.close()
        return num_char
